# 国内用rosdep有问题，需要加代理,即在某些链接前面加`https://ghproxy.com/`
**ros版本 - noetic**


## 1、 修改`/usr/lib/python3/dist-packages/rosdep2/sources_list.py`
```bash
sudo vim /usr/lib/python3/dist-packages/rosdep2/sources_list.py
```
在311行添加 `url="https://ghproxy.com/"+url`
![](PIC/01.png)

将72行的链接前面添加`https://ghproxy.com/`

![](PIC/04.png)

## 2、修改`/usr/lib/python3/dist-packages/rosdistro/__init__.py`
```bash
sudo vim /usr/lib/python3/dist-packages/rosdistro/__init__.py
```
将68行链接前面添加`https://ghproxy.com/`
![](PIC/02.png)


## 3、修改`/usr/lib/python3/dist-packages/rosdep2/gbpdistro_support.py`
```bash
sudo vim /usr/lib/python3/dist-packages/rosdep2/gbpdistro_support.py
```
将36行的链接前面添加`https://ghproxy.com/`

![](PIC/03.png)

## 4、修改`/usr/lib/python3/dist-packages/rosdep2/rep3.py`
```bash
sudo vim /usr/lib/python3/dist-packages/rosdep2/rep3.py
```
将39行的链接前面添加`https://ghproxy.com/`

![](PIC/05.png)

## 5、修改`/usr/lib/python3/dist-packages/rosdistro/manifest_provider/github.py`
```bash
sudo vim /usr/lib/python3/dist-packages/rosdistro/manifest_provider/github.py
```
将68和119行的链接前面添加`https://ghproxy.com/`

![](PIC/06.png)
![](PIC/07.png)


## 6、修改`/usr/lib/python3/dist-packages/rosdep2/gbpdistro_support.py`
```bash
sudo vim /usr/lib/python3/dist-packages/rosdep2/gbpdistro_support.py
```
在204行添加`gbpdistro_url = "https://ghproxy.com/" + gbpdistro_url`

![](PIC/08.png)


## 7、 测试有没有解决
```bash
sudo rosdep init
rosdep update
```